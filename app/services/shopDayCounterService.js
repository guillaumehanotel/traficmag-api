const moment = require('moment')
const ShopDayCounter = require('../models/shopdaycounter')
const Shop = require('../models/shop')
const { createItem } = require('../middleware/db')

const buildMetadata = async (shopId, doorId, date, increase) => {
  const passageDirection = increase ? 'entry' : 'exit'
  let metadata = {
    date,
    passageDirection
  }
  if (doorId === null) {
    const userId = await Shop.findById(shopId)
    metadata = {
      ...metadata,
      passageSource: 'manual',
      user: userId
    }
  } else {
    metadata = {
      ...metadata,
      passageSource: 'automatic',
      door: doorId
    }
  }
  return metadata
}

// eslint-disable-next-line max-statements
const increase = async (shopId, doorId = null) => {
  try {
    const start = moment().startOf('day')
    const end = moment().endOf('day')
    const date = new Date(Date.now()).toISOString()
    const shop = await Shop.findById(shopId)

    const shopDayCounterResponses = await ShopDayCounter.find({
      shop: shopId,
      date: {
        $gte: start,
        $lte: end
      }
    })

    let shopDayCounter

    let metadata = await buildMetadata(shopId, doorId, date, true)

    if (shopDayCounterResponses.length === 0) {
      const shopDayCounterData = {
        shop: shopId,
        date,
        currentPeopleNumber: 1,
        maxNumberPeople: 1,
        entryCounter: 1,
        nbAlert: 0,
        metadata: {
          ...metadata,
          currentPeopleNumber: 1
        }
      }
      shopDayCounter = await createItem(shopDayCounterData, ShopDayCounter)
    } else {
      shopDayCounter = shopDayCounterResponses[0]

      const currentPeopleNumber = shopDayCounter.currentPeopleNumber + 1

      let maxNumberPeople = shopDayCounter.maxNumberPeople
      if (currentPeopleNumber > maxNumberPeople) {
        maxNumberPeople++
      }

      let nbAlert = shopDayCounter.nbAlert
      if (currentPeopleNumber === shop.maxPeople) {
        nbAlert++
      }

      metadata = {
        ...metadata,
        currentPeopleNumber
      }

      await ShopDayCounter.updateOne(
        {
          shop: shopId,
          date: {
            $gte: start,
            $lte: end
          }
        },
        {
          currentPeopleNumber,
          entryCounter: shopDayCounter.entryCounter + 1,
          maxNumberPeople,
          nbAlert,
          $push: {
            metadata
          }
        }
      )

      shopDayCounter = (
        await ShopDayCounter.find({
          shop: shopId,
          date: {
            $gte: start,
            $lte: end
          }
        })
      )[0]
    }
    return shopDayCounter
  } catch (error) {
    console.error(error)
  }
}

// eslint-disable-next-line max-statements
const decrease = async (shopId, doorId = null) => {
  try {
    const start = moment().startOf('day')
    const end = moment().endOf('day')
    const date = new Date(Date.now()).toISOString()

    const shopDayCounterResponses = await ShopDayCounter.find({
      shop: shopId,
      date: {
        $gte: start,
        $lte: end
      }
    })

    let shopDayCounter

    let metadata = await buildMetadata(shopId, doorId, date, false)

    if (shopDayCounterResponses.length === 0) {
      const shopDayCounterData = {
        shop: shopId,
        date,
        currentPeopleNumber: 1,
        maxNumberPeople: 1,
        entryCounter: 1,
        metadata
      }
      shopDayCounter = await createItem(shopDayCounterData, ShopDayCounter)
    } else {
      shopDayCounter = shopDayCounterResponses[0]

      const currentPeopleNumber =
        shopDayCounter.currentPeopleNumber - 1 < 0
          ? 0
          : shopDayCounter.currentPeopleNumber - 1

      metadata = {
        ...metadata,
        currentPeopleNumber
      }

      await ShopDayCounter.updateOne(
        {
          shop: shopId,
          date: {
            $gte: start,
            $lte: end
          }
        },
        {
          currentPeopleNumber,
          exitCounter: shopDayCounter.exitCounter + 1,
          $push: {
            metadata
          }
        }
      )

      shopDayCounter = (
        await ShopDayCounter.find({
          shop: shopId,
          date: {
            $gte: start,
            $lte: end
          }
        })
      )[0]
    }
    return shopDayCounter
  } catch (error) {
    console.error(error)
  }
}

module.exports = { increase, decrease }
