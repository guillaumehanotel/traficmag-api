const express = require('express')
const router = express.Router()
require('../../config/passport')
const passport = require('passport')
const requireAuth = passport.authenticate('jwt', {
  session: false
})
const trimRequest = require('trim-request')

// const { roleAuthorization } = require('../controllers/auth')

const {
  getDoors,
  createDoor,
  getDoor,
  updateDoor,
  deleteDoor
} = require('../controllers/doors')

const {
  validateCreateDoor,
  validateGetDoor,
  // validateUpdateDoor,
  validateDeleteDoor
} = require('../controllers/doors/validators')

/*
 * Doors routes
 */

/*
 * Get items route
 */
router.get(
  '/',
  requireAuth,
  // roleAuthorization(['admin']),
  trimRequest.all,
  getDoors
)

/*
 * Create new item route
 */
router.post(
  '/',
  requireAuth,
  // roleAuthorization(['admin']),
  trimRequest.all,
  validateCreateDoor,
  createDoor
)

/*
 * Get item route
 */
router.get(
  '/:id',
  requireAuth,
  // roleAuthorization(['admin']),
  trimRequest.all,
  validateGetDoor,
  getDoor
)

/*
 * Update item route
 */
router.patch(
  '/:id',
  requireAuth,
  // roleAuthorization(['admin']),
  trimRequest.all,
  // validateUpdateUser,
  updateDoor
)

/*
 * Delete item route
 */
router.delete(
  '/:id',
  requireAuth,
  // roleAuthorization(['admin']),
  trimRequest.all,
  validateDeleteDoor,
  deleteDoor
)

module.exports = router
