const express = require('express')
const router = express.Router()
require('../../config/passport')
const passport = require('passport')
const requireAuth = passport.authenticate('jwt', {
  session: false
})
const trimRequest = require('trim-request')

const {
  getShops,
  createShop,
  getShop,
  deleteShop
} = require('../controllers/shops')

const {
  validateCreateShop,
  validateGetShop,
  validateDeleteShop
} = require('../controllers/shops/validators')

/*
 * Shops routes
 */

/*
 * Get items route
 */
router.get('/', requireAuth, trimRequest.all, getShops)

/*
 * Create new item route
 */
router.post('/', requireAuth, trimRequest.all, validateCreateShop, createShop)

/*
 * Get item route
 */
router.get('/:id', requireAuth, trimRequest.all, validateGetShop, getShop)

/*
 * Delete item route
 */
router.delete(
  '/:id',
  requireAuth,
  trimRequest.all,
  validateDeleteShop,
  deleteShop
)

module.exports = router
