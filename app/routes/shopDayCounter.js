const express = require('express')
const router = express.Router()
require('../../config/passport')
const passport = require('passport')
const requireAuth = passport.authenticate('jwt', {
  session: false
})
const trimRequest = require('trim-request')

const {
  getShopDayCounters,
  getTodayShopDayCounter,
  increaseShopDayCounter,
  decreaseShopDayCounter
} = require('../controllers/shopdaycounters')

/*
 * ShopDayCounter routes
 */

/*
 * Get items route
 */
router.get('/', requireAuth, trimRequest.all, getShopDayCounters)

/*
 * Get item route
 */
router.get('/today', requireAuth, trimRequest.all, getTodayShopDayCounter)

/*
 * Increase
 */
router.get(
  '/today/increase',
  requireAuth,
  trimRequest.all,
  increaseShopDayCounter
)

/*
 * Decrease
 */
router.get(
  '/today/decrease',
  requireAuth,
  trimRequest.all,
  decreaseShopDayCounter
)

module.exports = router
