const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate-v2')

const MetadataSchema = new mongoose.Schema({
  date: Date,
  passageDirection: String, // entry or exit
  passageSource: String, // manual or automatic
  currentPeopleNumber: {
    type: Number,
    default: 0
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  door: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Door'
  }
})

const ShopDayCounterSchema = new mongoose.Schema({
  shop: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Shop'
  },
  date: {
    type: Date,
    required: true
  },
  currentPeopleNumber: {
    type: Number,
    default: 0
  },
  maxNumberPeople: {
    type: Number,
    default: 0
  },
  nbAlert: {
    type: Number,
    default: 0
  },
  entryCounter: {
    type: Number,
    default: 0
  },
  exitCounter: {
    type: Number,
    default: 0
  },
  metadata: {
    type: [MetadataSchema],
    default: {}
  }
})

ShopDayCounterSchema.plugin(mongoosePaginate)
module.exports = mongoose.model('ShopDayCounter', ShopDayCounterSchema)
