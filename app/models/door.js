const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate-v2')

const DoorSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  slug: {
    type: String,
    required: true
  },
  mqttTopicName: {
    type: String
  },
  shop: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Shop'
  }
})

DoorSchema.plugin(mongoosePaginate)
module.exports = mongoose.model('Door', DoorSchema)
