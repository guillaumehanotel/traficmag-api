const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate-v2')

const ShopSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  slug: {
    type: String,
    required: true
  },
  area: {
    type: Number
  },
  maxPeople: {
    type: Number,
    required: true
  },
  city: {
    type: String
  },
  postalCode: {
    type: String
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  doors: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Door'
    }
  ]
})

ShopSchema.plugin(mongoosePaginate)
module.exports = mongoose.model('Shop', ShopSchema)
