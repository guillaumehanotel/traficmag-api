const ShopDayCounter = require('../../models/shopdaycounter')
const { createItem } = require('../../middleware/db')
const { handleError } = require('../../middleware/utils')
const moment = require('moment')

/**
 * Get item function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
const getTodayShopDayCounter = async (req, res) => {
  try {
    const start = moment().startOf('day')
    const end = moment().endOf('day')

    const shopDayCounterResponses = await ShopDayCounter.find({
      shop: req.query.shop,
      date: {
        $gte: start,
        $lte: end
      }
    })

    let shopDayCounter

    // Si pas de résultat, il faut ajouter l'entrée du jour
    if (shopDayCounterResponses.length === 0) {
      const date = new Date(Date.now()).toISOString()
      const shopDayCounterData = {
        shop: req.query.shop,
        date
      }
      shopDayCounter = await createItem(shopDayCounterData, ShopDayCounter)
    } else {
      shopDayCounter = shopDayCounterResponses[0]
    }

    res.status(200).json(shopDayCounter)
  } catch (error) {
    handleError(res, error)
  }
}

module.exports = { getTodayShopDayCounter }
