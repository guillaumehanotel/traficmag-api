const ShopDayCounter = require('../../models/shopdaycounter')
const { handleError } = require('../../middleware/utils')
const { getItems, createItem } = require('../../middleware/db')
const moment = require('moment')

/**
 * Get items function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
const getShopDayCounters = async (req, res) => {
  try {
    const start = moment().startOf('day')
    const end = moment().endOf('day')

    const shopDayCounterResponses = await ShopDayCounter.find({
      shop: req.query.shop,
      date: {
        $gte: start,
        $lte: end
      }
    })

    if (shopDayCounterResponses.length === 0) {
      const date = new Date(Date.now()).toISOString()
      const shopDayCounterData = {
        shop: req.query.shop,
        date
      }
      await createItem(shopDayCounterData, ShopDayCounter)
    }

    res.status(200).json(await getItems(req, ShopDayCounter, req.query))
  } catch (error) {
    handleError(res, error)
  }
}

module.exports = { getShopDayCounters }
