const shopDayCounterService = require('../../services/shopDayCounterService')
const { handleError } = require('../../middleware/utils')

/**
 * Get items function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
const decreaseShopDayCounter = async (req, res) => {
  try {
    const shopDayCounter = await shopDayCounterService.decrease(req.query.shop)
    res.status(200).json(shopDayCounter)
  } catch (error) {
    handleError(res, error)
  }
}

module.exports = { decreaseShopDayCounter }
