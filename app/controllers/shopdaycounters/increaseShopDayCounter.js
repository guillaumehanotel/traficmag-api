const shopDayCounterService = require('../../services/shopDayCounterService')
const { handleError } = require('../../middleware/utils')

/**
 * Get items function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
const increaseShopDayCounter = async (req, res) => {
  try {
    const shopDayCounter = await shopDayCounterService.increase(req.query.shop)
    res.status(200).json(shopDayCounter)
  } catch (error) {
    handleError(res, error)
  }
}

module.exports = { increaseShopDayCounter }
