const { validateCreateDoor } = require('./validateCreateDoor')
const { validateDeleteDoor } = require('./validateDeleteDoor')
const { validateGetDoor } = require('./validateGetDoor')

module.exports = {
  validateCreateDoor,
  validateDeleteDoor,
  validateGetDoor
}
