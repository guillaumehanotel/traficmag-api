const Door = require('../../models/door')
const Shop = require('../../models/shop')
const { slugify } = require('../../middleware/utils')
const { createItem, updateItem } = require('../../middleware/db')
const { handleError } = require('../../middleware/utils')
const { matchedData } = require('express-validator')
const { doorExists } = require('./helpers')

/**
 * Create item function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
const createDoor = async (req, res) => {
  try {
    req = matchedData(req)
    const shop = await Shop.findById(req.shop)
    req = {
      ...req,
      slug: slugify(req.name)
    }
    const doesDoorExists = await doorExists(req.name, req.shop)
    if (!doesDoorExists) {
      const newDoor = await createItem(req, Door)
      const updatedDoor = {
        mqttTopicName: `${shop.slug}_${shop._id}/${newDoor.slug}_${newDoor._id}`
      }
      res.status(201).json(await updateItem(newDoor._id, Door, updatedDoor))
    }
  } catch (error) {
    handleError(res, error)
  }
}

module.exports = { createDoor }
