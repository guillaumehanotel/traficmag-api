const Door = require('../../models/door')

const { handleError } = require('../../middleware/utils')
const { getItems } = require('../../middleware/db')

/**
 * Get items function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
const getDoors = async (req, res) => {
  try {
    res.status(200).json(await getItems(req, Door, req.query))
  } catch (error) {
    handleError(res, error)
  }
}

module.exports = { getDoors }
