const Door = require('../../../models/door')
const { buildErrObject } = require('../../../middleware/utils')
const ObjectId = require('mongoose').Types.ObjectId

/**
 * Checks if a city already exists in database
 * @param {string} name - name of item
 * @param shopId
 */
const doorExists = (name = '', shopId) => {
  return new Promise((resolve, reject) => {
    Door.findOne(
      {
        name,
        shopId: new ObjectId(shopId)
      },
      (err, item) => {
        if (err) {
          return reject(buildErrObject(422, err.message))
        }

        if (item) {
          return reject(buildErrObject(422, 'DOOR_ALREADY_EXISTS'))
        }
        resolve(false)
      }
    )
  })
}

module.exports = { doorExists }
