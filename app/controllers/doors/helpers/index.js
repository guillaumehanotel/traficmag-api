const { doorExists } = require('./doorExists')

module.exports = {
  doorExists
}
