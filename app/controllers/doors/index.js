const { createDoor } = require('./createDoor')
const { deleteDoor } = require('./deleteDoor')
const { getDoor } = require('./getDoor')
const { getDoors } = require('./getDoors')
const { updateDoor } = require('./updateDoor')

module.exports = {
  createDoor,
  deleteDoor,
  getDoor,
  getDoors,
  updateDoor
}
