const Shop = require('../../models/shop')
const { slugify } = require('../../middleware/utils')
const { createItem } = require('../../middleware/db')
const { handleError } = require('../../middleware/utils')
const { shopExists } = require('./helpers')
const { matchedData } = require('express-validator')

/**
 * Create item function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
const createShop = async (req, res) => {
  try {
    req = matchedData(req)
    req = {
      ...req,
      maxPeople: req.area > 8 ? Math.round(req.area / 8) : 1,
      slug: slugify(req.name)
    }
    const doesShopExists = await shopExists(req.name, req.user)
    if (!doesShopExists) {
      res.status(201).json(await createItem(req, Shop))
    }
  } catch (error) {
    handleError(res, error)
  }
}

module.exports = { createShop }
