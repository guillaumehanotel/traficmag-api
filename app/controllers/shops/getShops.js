const Shop = require('../../models/shop')

const { handleError } = require('../../middleware/utils')
const { getItems } = require('../../middleware/db')

/**
 * Get items function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
const getShops = async (req, res) => {
  try {
    res.status(200).json(await getItems(req, Shop, req.query))
  } catch (error) {
    handleError(res, error)
  }
}

module.exports = { getShops }
