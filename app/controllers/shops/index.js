const { createShop } = require('./createShop')
const { deleteShop } = require('./deleteShop')
const { getShop } = require('./getShop')
const { getShops } = require('./getShops')
const { updateShop } = require('./updateShop')

module.exports = {
  createShop,
  deleteShop,
  getShop,
  getShops,
  updateShop
}
