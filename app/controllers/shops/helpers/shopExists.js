const Shop = require('../../../models/shop')
const { buildErrObject } = require('../../../middleware/utils')
const ObjectId = require('mongoose').Types.ObjectId

/**
 * Checks if a city already exists in database
 * @param {string} name - name of item
 * @param userId
 */
const shopExists = (name = '', userId) => {
  return new Promise((resolve, reject) => {
    Shop.findOne(
      {
        name,
        userId: new ObjectId(userId)
      },
      (err, item) => {
        if (err) {
          return reject(buildErrObject(422, err.message))
        }

        if (item) {
          return reject(buildErrObject(422, 'SHOP_ALREADY_EXISTS'))
        }
        resolve(false)
      }
    )
  })
}

module.exports = { shopExists }
