const { validateCreateShop } = require('./validateCreateShop')
const { validateDeleteShop } = require('./validateDeleteShop')
const { validateGetShop } = require('./validateGetShop')

module.exports = {
  validateCreateShop,
  validateDeleteShop,
  validateGetShop
}
