const mqtt = require('mqtt')
const mqttClient = mqtt.connect(process.env.MQTT_SERVER)
const Shop = require('./app/models/shop')
const Door = require('./app/models/door')
const shopDayCounterService = require('./app/services/shopDayCounterService')

const getMqttTopics = async () => {
  const topics = []
  const shops = await Shop.find()
  for (const shop of shops) {
    const doors = await Door.find({
      shop: shop._id
    })
    for (const door of doors) {
      topics.push({
        shopId: shop._id,
        doorId: door._id,
        topicName: `${shop.slug}_${shop._id}/${door.slug}_${door._id}`
      })
    }
  }
  return topics
}

const handleMqttMessage = async (shopId, doorId, mqttMessage) => {
  if (mqttMessage === 'INCREASE') {
    await shopDayCounterService.increase(shopId, doorId)
  } else if (mqttMessage === 'DECREASE') {
    await shopDayCounterService.decrease(shopId, doorId)
  }
}

const listenMqttTopics = async (mqttTopics) => {
  for (const mqttTopic of mqttTopics) {
    mqttClient.on('connect', () => {
      mqttClient.subscribe(mqttTopic.topicName)
    })
  }
  mqttClient.on('message', async (topic, message) => {
    const shopId = topic.split('/')[0].split('_')[1]
    const doorId = topic.split('/')[1].split('_')[1]
    await handleMqttMessage(shopId, doorId, message.toString())
  })
}

const listenDoorsPassageEvents = async () => {
  const mqttTopics = await getMqttTopics()
  await listenMqttTopics(mqttTopics)
}

exports.listenDoorsPassageEvents = listenDoorsPassageEvents()
