#!/bin/bash

set -f

# Delete Old Repo
rm -rf /home/guillaumehanotel/traficmag-api

# Clone repo again
git clone https://gitlab.com/guillaumehanotel/traficmag-api.git

# Install App
cd /home/guillaumehanotel/traficmag-api
cp .env.example .env
npm install

# Restart process
pm2 restart 0
